#!/usr/bin/env bats

setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'
    
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"
    # make executables in src/ visible to PATH
    PATH="$DIR/../src:$PATH"
}

pipe_summ() {
  cat "$1" | summ
}

@test "no lines" {
    run pipe_summ <(echo "")
    assert_output ""
}

@test "one line" {
    run pipe_summ <(echo "Welcome")
    assert_output "Welcome"
}

@test "two lines" {
    run summ << EOF
One line
Two lines
EOF

  assert_output - << EOF
One line
Two lines
EOF
}

@test "two lines, collapsed" {
    SUMM_COLLAPSE=2 run summ << EOF
One line
One lines
EOF

  assert_output "One line[...2 times]"
}

@test "two lines at start, collapsed with trailing line" {
    SUMM_COLLAPSE=2 run summ << EOF
One line
One lines
NON MATCHING
EOF

  assert_output - << EOF
One line[...2 times]
NON MATCHING
EOF
}

@test "two lines at end, collapsed with prefixed line" {
    SUMM_COLLAPSE=2 run summ << EOF
NON MATCHING
One line
One lines
EOF

  assert_output - << EOF
NON MATCHING
One line[...2 times]
EOF
}

@test "two lines in middle, collapsed with lines around" {
    SUMM_COLLAPSE=2 run summ << EOF
NON MATCHING
One line
One lines
NON MATCHING
EOF

  assert_output - << EOF
NON MATCHING
One line[...2 times]
NON MATCHING
EOF
}

@test "nested possible collapses that don't reach threshold" {
    SUMM_COLLAPSE=3 run summ << EOF
One line
One lines
One lines and a bit
One lines and a bit
One lines
EOF

  assert_output - << EOF
One line[...5 times]
EOF
}

@test "multiple collapses" {
    SUMM_COLLAPSE=3 run summ << EOF
1d0
< /home
43a43,45
> /home/.bash_history
> /home/.bash_logout
> /home/.bash_profile
55a60,69
> /home/.config/bin
> /home/.config/bin/config.json
> /home/.config/cni
> /home/.config/cni/net.d
> /home/.config/cni/net.d/cni.lock
> /home/.config/dconf
> /home/.config/dconf/user
> /home/.config/enchant
> /home/.config/enchant/en_GB.dic
> /home/.config/enchant/en_GB.exc
85a100,102
> /home/.config/gnome-initial-setup-done
> /home/.config/goa-1.0
> /home/.config/.gsd-keyboard.settings-ported
90a108,110
> /home/.config/gtk-3.0
> /home/.config/gtk-3.0/bookmarks
> /home/.config/gtk-4.0
92a113,118
> /home/.config/ibus
> /home/.config/ibus/bus
> /home/.config/ibus/bus/2dea1c49e80c43bf9a21cab53e86c580-unix-0
> /home/.config/klavaro
> /home/.config/klavaro/altcolor.ini
> /home/.config/klavaro/preferences.ini
101a128,136
> /home/.config/libvirt
> /home/.config/libvirt/qemu
> /home/.config/libvirt/qemu/checkpoint
> /home/.config/libvirt/qemu/dump
> /home/.config/libvirt/qemu/lib
> /home/.config/libvirt/qemu/nvram
> /home/.config/libvirt/qemu/ram
> /home/.config/libvirt/qemu/save
> /home/.config/libvirt/qemu/snapshot
104a140,141
> /home/.config/nautilus
> /home/.config/nautilus/search-metadata
EOF

  assert_output - << EOF
1d0
< /home
43a43,45
> /home/.bash_history
> /home/.bash_logout
> /home/.bash_profile
55a60,69
> /home/.config/bin
> /home/.config/bin/config.json
> /home/.config/cni[...3 times]
> /home/.config/dconf
> /home/.config/dconf/user
> /home/.config/enchant[...3 times]
85a100,102
> /home/.config/gnome-initial-setup-done
> /home/.config/goa-1.0
> /home/.config/.gsd-keyboard.settings-ported
90a108,110
> /home/.config/gtk-3.0
> /home/.config/gtk-3.0/bookmarks
> /home/.config/gtk-4.0
92a113,118
> /home/.config/ibus[...3 times]
> /home/.config/klavaro[...3 times]
101a128,136
> /home/.config/libvirt[...9 times]
104a140,141
> /home/.config/nautilus
> /home/.config/nautilus/search-metadata
EOF
}

