# Summ

Summ shortens long outputs by summarising lines that start with the same content.

Summ works best on log files and output from tools such as `diff` that have some
kind of order. It was created to save me time reading long diffs of filesystems.

## Background

Summ is short for summary. I often find myself comparing backups against existing
filesystems with `diff`. There can be many small files in cache dirs
and I don't need to see every single one, just a summary that there is such a
dir and it has many files in it is enough. This tool was created to give me that
overview. It can reduce a diff of 10,000s of lines to just a few 100s.

## Install

There are two ways:

### Via bin

**RECOMMENDED** Install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/summ
```

bin will allow you to manage the install and update in future.

### Via git clone

Git clone the repository and copy summ onto your path. For example:

```bash
git clone https://gitlab.com/IanTwenty/summ
cp summ/src/summ /usr/local/bin
```

## Usage

Summ will read from stdin and output to stdout:

```bash
diff /home /mnt/backup | summ
```

Summ will collapse similar lines wherever 40 are seen in succession, suffixing
them with `[...NNN times]`:

```txt
...
> /home/user/.local/share/flatpak[...11907 times]
...
> /home/user/.local/share/gnome-shell[...417 times]
...
```

To change the limit set `SUMM_COLLAPSE`:

```bash
diff /home /mnt/backup | SUMM_COLLAPSE=5 summ
```

To enable debug mode, just set `DEBUG` to any value:

```bash
journalctl | DEBUG=x summ
```

So far debug mode just outputs progress (lines processed) to stderr.

## License

Here is a summary:

- Source code is licensed under GPL-3.0-or-later.
- Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
- Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

Summ is free software: you can redistribute it and modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
